package br.com.waiso.autenticacao.controller;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import br.com.massuda.alexander.autenticacao.dao.ISistemaDAO;
import br.com.massuda.alexander.autenticacao.dao.finder.IFinderSistema;
import br.com.massuda.alexander.autenticacao.dao.finder.impl.FinderSistemaImpl;
import br.com.massuda.alexander.autenticacao.dao.impl.SistemaDAOImpl;
import br.com.massuda.alexander.autenticacao.orm.modelo.sistema.Sistema;
import br.com.waiso.framework.json.JSONReturn;

public class SistemaCRUD extends Controlador<JSONReturn> {

	ISistemaDAO dao = new SistemaDAOImpl();
	IFinderSistema finder = new FinderSistemaImpl();
	
	public JSONReturn incluir (ServletRequest requisicao, ServletResponse resposta) {
		Sistema sistema = new Sistema();
		String nome = getStringDaRequisicao(requisicao, "nome");
		sistema.setNome(nome);
		
		sistema = dao.incluir(sistema);
		
		return SUCESSO(sistema);
	}
	
	public JSONReturn listar (ServletRequest requisicao, ServletResponse resposta) {
		List<Sistema> sistemas = new FinderSistemaImpl().listar();
		return SUCESSO(sistemas);
	}
	
	public JSONReturn pesquisar (ServletRequest requisicao, ServletResponse resposta) {
		Long id = null;
		String strID = getStringDaRequisicao(requisicao, "id");
		
		try {
			id = Long.parseLong(strID);
		} catch (NumberFormatException nfe) {
			return ERRO(nfe);
		}
		
		Sistema sistema = new FinderSistemaImpl().pesquisar(id);
		return SUCESSO(sistema);
	}
	
	public JSONReturn pesquisarPorNomeComo (ServletRequest requisicao, ServletResponse resposta) {
		String nome = getStringDaRequisicao(requisicao, "nome");
		List<Sistema> sistema = new FinderSistemaImpl().pesquisarPorNomeComo(nome);
		return SUCESSO(sistema);
	}
	
	public JSONReturn excluir (ServletRequest requisicao, ServletResponse resposta) {
		String strId = getStringDaRequisicao(requisicao, "id");
		Long id = 0l;
		try {
			id = Long.parseLong(strId);
			Sistema o = finder.pesquisar(id);
			dao.excluir(o);
		}catch (NumberFormatException e){
			e.printStackTrace();
		}
		return SUCESSO("");
	}
	
}
