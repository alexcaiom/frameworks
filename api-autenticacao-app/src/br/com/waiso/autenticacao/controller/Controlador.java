package br.com.waiso.autenticacao.controller;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.massuda.alexander.autenticacao.excecoes.ErroNegocio;
import br.com.waiso.autenticacao.utils.Sessao;
import br.com.waiso.framework.abstratas.Classe;
import br.com.waiso.framework.json.Consequence;
import br.com.waiso.framework.json.JSONReturn;
import br.com.waiso.framework.view.Servlet;

public class Controlador<T> extends Servlet<T> {
	
	protected String[] camposObrigatorios = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 5975259121544413638L;
	
	protected String resposta (Consequence status, Object dado) {
		return JSONReturn.newInstance(status, dado).serialize();
	}
	
	protected JSONReturn SUCESSO (Object dado) {
		return JSONReturn.newInstance(Consequence.SUCCESSO, dado);
	}
	
	protected JSONReturn SUCESSO (Object dado, String...menos) {
		return JSONReturn.newInstance(Consequence.SUCCESSO, dado).exclude(menos);
	}
	
	protected JSONReturn ATENCAO (Object dado) {
		return JSONReturn.newInstance(Consequence.ATENCAO, dado);
	}
	
	protected JSONReturn DESLOGADO_COM_SUCESSO (Object dado) {
		return JSONReturn.newInstance(Consequence.DESLOGADO_COM_SUCESSO, dado);
	}
	
	protected JSONReturn ERRO (Object dado) {
		return JSONReturn.newInstance(Consequence.ERRO, dado);
	}
	
	protected JSONReturn MUITOS_ERROS (Object dado) {
		return JSONReturn.newInstance(Consequence.MUITOS_ERROS, dado);
	}
	
	protected JSONReturn PARE_DE_TENTAR_NOVAMENTE (Object dado) {
		return JSONReturn.newInstance(Consequence.PARE_DE_TENTAR_NOVAMENTE, dado);
	}
	
	protected JSONReturn SEM_ACESSO (Object dado) {
		return JSONReturn.newInstance(Consequence.SEM_ACESSO, dado);
	}
	
	protected JSONReturn TENTE_NOVAMENTE (Object dado) {
		return JSONReturn.newInstance(Consequence.TENTE_NOVAMENTE, dado);
	}

	protected void setCamposObrigatorios(String... campos) {
		camposObrigatorios = campos;
	}
	
	protected void validaCamposObrigatorios(ServletRequest requisicao, ServletResponse resposta) {
		for (String nomeCampo : camposObrigatorios) {
			if (Classe.naoExiste(getStringDaRequisicao(requisicao, nomeCampo))) {
				throw new ErroNegocio("Campo Obrigatório não informado: " + nomeCampo);
			}
		}
		
	}
	
	protected void validaCamposObrigatorios(ServletRequest requisicao, ServletResponse resposta, String... campos) {
		setCamposObrigatorios(campos);
		validaCamposObrigatorios(requisicao, resposta);
	}
	
	protected Object getAtributoDaSessao(String nome){
		return Sessao.get(nome);
	}
	
	protected String getStringDaRequisicao(ServletRequest requisicao, String chave) {
		if (Classe.existe(requisicao) && Classe.existe(requisicao.getParameter(chave))) {
			return (String) requisicao.getParameter(chave);
		}
		return null;
	}
	
	protected boolean existe(Object o){
		return Classe.existe(o);
	}

	protected void addNaSessao(ServletRequest requisicao, String nome, Object dado) {
		HttpSession sessaoHttp = ((HttpServletRequest) requisicao).getSession();
		sessaoHttp.setAttribute(nome, dado);
	}

}
