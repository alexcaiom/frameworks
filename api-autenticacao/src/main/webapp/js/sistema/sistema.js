/**
 * 
 */

var sistema = {
		init : function() {
			debugger;
			sistema.listar();
			sistema.initEventos();
		},
		initEventos : function() {
			$("#nome").keyup(function(e) {
				if (e.keyCode == 13) {
					$("#btnCadastrar").click();
				}
			});
			$("#btnCadastrar").on('click', function() {
				var dadosDoFormularioEstaoValidos = sistema.validar();
				if (dadosDoFormularioEstaoValidos) {
					sistema.gravar();
				}
			});
			$("#btnPesquisar").on('click', function() {
				var textoPesquisa = $("#nomePesquisa").val();
				sistema.pesquisar(textoPesquisa);
			});
			$("#nomePesquisa").keyup(function() {
				var textoPesquisa = $("#nomePesquisa").val();
				sistema.pesquisar(textoPesquisa);
			});
		},
		gravar : function() {
			exibir($("#msgCarregando"));
			ocultar($("#lblSemResultados"));
			ocultar($("#tbl.dados"));
			
			var metodo = all.metodoPOST;
			url = all.url + "/sistema";
			var tipo = "application/json";
			var nome = $("#nome").val();
			var data = {
				"nome" : nome
			};
			
			var funcoes = {
					sucesso : function(retorno){
						sistema.listar();
						log(retorno);
					},
					erro : function(retorno){
						if (retorno.localizedMessage != null) {
							$("#mensagem").html(retorno.localizedMessage);
							$("#mensagem").addClass("erro");
							$("#mensagem").show();
						}
					},
					
					atencao : function() {
						if (retorno.dado != null) {
							$("#mensagem").html(retorno.dado);
							$("#mensagem").addClass("aviso");
							$("#mensagem").show();
						}
					}
			};
			
			
			all.Ajax(url, metodo, all.assincrono, funcoes, data, tipo, tipo);
		},
		excluir : function(id) {
			exibir($("#msgCarregando"));
			ocultar($("#lblSemResultados"));
			ocultar($("#tbl.dados"));
			var classe = "Sistema";
			var metodo = "excluir";
			var textoPesquisa = $("#nomePesquisa").val();
			$.ajax(
					{
						url : url,
						async : ajaxAsyncPadrao,
						data : {
							classe : classe,
							metodo : metodo,
							id   : id
						}
					}
			)
			.done(function(retorno){
				retorno = all.converterTextoEmJSON(retorno);
				var consequencia = retorno.consequence;
				if(consequencia === "SUCCESSO") {
					var dado = retorno.dado;
					if (existe(dado)) {
						sistema.pesquisar(textoPesquisa);
					}
				} else if (consequencia == "ERRO") {
					if (retorno.localizedMessage != null) {
						$("#mensagem").html(retorno.localizedMessage);
						$("#mensagem").addClass("erro");
						$("#mensagem").show();
					}
				} else if (consequencia == "ATENCAO") {
					if (retorno.dado != null) {
						$("#mensagem").html(retorno.dado);
						$("#mensagem").addClass("aviso");
						$("#mensagem").show();
					}
				} 
				console.log(retorno);
			})
			.fail(function(retorno){
				retorno = all.converterTextoEmJSON(retorno);
				console.log(retorno);
			});
		},
		validar : function() {
			var formValido = $("#nome").val() != '';
			return formValido;
		},
		validarPesquisa : function() {
			var pesquisaValido = $("#nomePesquisa").val() != '';
			return pesquisaValido;
		},
		preencherTabela : function(dado) {
			sistema.resetTabelaSistema();
			for (var i = 0; i < dado.length; i++) {
				$("#tbl").append(sistema.getLinhaTabelaSistema(dado[i]));
			}
			
			sistema.atribuirEventosDeSelecaoAsLinhasDaTabela();
			ocultar($("#msgCarregando"));
			var quantidadeDeRegistros = $("tr[data-id]").length;
			if (quantidadeDeRegistros == 0) {
				ocultar($("#tbl.dados"));
				exibir($("#lblSemResultados"));
			} else {
				exibir($("#tbl.dados"));
				ocultar($("#msgCarregando"));
			}
		},
		listar : function() {
			exibir($("#msgCarregando"));
			ocultar($("#tbl.dados"));
			var metodo = all.metodoGET;
			
			var crossDomain = false;
			var endereco = all.url+"/sistema/";
			var data = {};
			var fnSucesso = function(retorno){
				debugger;
				retorno = all.converterTextoEmJSON(retorno);
				var dado = retorno;
				if (dado != null) {
					sistema.preencherTabela(dado);
				}
				console.log(retorno);
			};
			
			var fnAtencao = function(retorno) {
				$("#mensagem").html(retorno);
				$("#mensagem").addClass("aviso");
				$("#mensagem").show();
			};
			
			var fnErro = function(retorno){
				debugger;
//		retorno = all.converterTextoEmJSON(retorno);
				console.log(retorno);
				
				
//		if (retorno.localizedMessage != null) {
//			$("#mensagem").html(retorno.localizedMessage);
//			$("#mensagem").addClass("erro");
//			$("#mensagem").show();
//		}
			};
			
			var funcoes = {
				sucesso : fnSucesso,
				atencao : fnAtencao,
				erro    : fnErro
			};
			
			all.getJSON(endereco, metodo, all.assincrono, funcoes, data, crossDomain);
		},
		pesquisar : function(textoPesquisa) {
			exibir($("#msgCarregando"));
			ocultar($("#tbl.dados"));
			
			var crossDomain = false;
			var url = all.url+"/sistema/"+textoPesquisa;
			var fnSucesso = function(retorno){
				retorno = all.converterTextoEmJSON(retorno);
				var consequencia = retorno.consequence;
				if(consequencia === "SUCCESSO") {
					var dado = retorno.dado;
					if (existe(dado)) {
						sistema.preencherTabela(dado);
					}
				} else if (consequencia == "ERRO") {
					if (retorno.localizedMessage != null) {
						$("#mensagem").html(retorno.localizedMessage);
						$("#mensagem").addClass("erro");
						$("#mensagem").show();
					}
				} else if (consequencia == "ATENCAO") {
					if (retorno.dado != null) {
						$("#mensagem").html(retorno.dado);
						$("#mensagem").addClass("aviso");
						$("#mensagem").show();
					}
				} 
				console.log(retorno);
			};
			
			var fnErro = function(retorno){
				retorno = all.converterTextoEmJSON(retorno);
				console.log(retorno);
			};
			
			all.getJSON(url, metodo, all.assincrono, fnSucesso, fnErro, data, crossDomain);
		},
		getLinhaTabelaSistema : function(dado) {
			var linha = "";
			
			linha = "<tr data-id="+dado.id+">";
			linha += 	"<td>";
			linha += 		dado.id;
			linha += 	"</td>";
			linha += 	"<td>";
			linha += 		dado.nome;
			linha += 	"</td>";
			linha += 	"<td>";
			linha += 		"<img src='img/excluir.gif' id='excluir' data-id='"+dado.id+"' title='excluir' />";
			linha += 	"</td>";
			linha += "</tr>";
			
			return linha;
		},
		atribuirEventosDeSelecaoAsLinhasDaTabela : function() {
			$("tr[data-id]").on('click', function() {
				var id = $(this).data('id');
				sistema.selecionarSistema(id);
			});
			
			$("img[data-id]").on('click', function() {
				debugger;
				var id = $(this).attr('data-id');
				if (confirm("Deseja mesmo excluir?")) {
					sistema.excluir(id);
				}
			});
		},
		selecionarSistema : function(id) {
			
			var crossDomain = false;
			var url = all.url + "/sistema/"+id;
			var fnSucesso = function(retorno){
				retorno = all.converterTextoEmJSON(retorno);
				var consequencia = retorno.consequence;
				if(consequencia === "SUCCESSO") {
					var sistema = retorno.dado;
					if (sistema != null) {
						$("#id").val(sistema.id);
						$("#nome").val(sistema.nome);
					}
				} else if (consequencia == "ERRO") {
					if (retorno.localizedMessage != null) {
						$("#mensagem").html(retorno.localizedMessage);
						$("#mensagem").addClass("erro");
						$("#mensagem").show();
					}
				} else if (consequencia == "ATENCAO") {
					if (retorno.dado != null) {
						$("#mensagem").html(retorno.dado);
						$("#mensagem").addClass("aviso");
						$("#mensagem").show();
					}
				} 
				console.log(retorno);
			};
			
			var fnErro = function(retorno){
				retorno = all.converterTextoEmJSON(retorno);
				console.log(retorno);
			};
			
			all.getJSON(url, metodo, all.assincrono, fnSucesso, fnErro, data, crossDomain);
		},
		resetTabelaSistema : function() {
			$("#tbl").html('');
			var tabelaInicial = "";
			
			tabelaInicial += "<thead>";
			tabelaInicial += 	"<tr>";
			tabelaInicial += 		"<td>";
			tabelaInicial += 			"Id";
			tabelaInicial += 		"</td>";
			tabelaInicial += 		"<td>";
			tabelaInicial += 			"Nome";
			tabelaInicial += 		"</td>";
			tabelaInicial += 		"<td>";
			tabelaInicial += 			"";
			tabelaInicial += 		"</td>";
			tabelaInicial += 	"</tr>";
			tabelaInicial += "</thead>";
			
			$("#tbl").html(tabelaInicial);
		}
};









