/**
 * 
 */

var all = {
		/*
		 * Variaveis
		 */
		metodoGET  : 	"GET",
		metodoPOST : 	"POST",
		assincrono : 	true,
		sincrono   : 	false,
		paginaAtual: 	"",
		paginaDestino: 	"",
		tipoJSON   :	"application/json",
		tipoPagina :	"text/html",
		debugar: 		false,
		
		url : '/api-autenticacao/rest',
		
		/**
		 * Metodos
		 */
		
		/**
		 * executa uma requisição ao servidor
		 * @param url
		 * @param metodo (GET, POST)
		 * @param assincrono (sincrono/assincrono)
		 * @param funcoes (fnSucesso, fnAtencao, fnErro)
		 * @param data (dados no corpo do POST)
		 * @param tipo (tipo de conteudo da requisicao
		 */
		Ajax : function(url, metodo, assincrono, funcoes, data, tipo, aceita) {
			if (typeof metodo === undefined) {
				metodo = all.metodoGET;
			}
			
			if (typeof erro === undefined) {
				erro = all.fnFalhaPadrao;
			}
			
			if (typeof data === undefined) {
				data = {
						
				};
			}
			
			if (all.metodoPOST === metodo) {
				data = JSON.stringify(data);
			}
			$.ajax({
				url : url,
				method : metodo,
				async : assincrono,
				data : data,
				statusCode : all.getTratamentosAjax(funcoes),
				"Content-Type" : tipo,
				"Accept" : aceita
			});
		},
		
		getTratamentosAjax : function(funcoes) {
			return {
				200 : function(resposta) {
					funcoes.sucesso(resposta);
				},
				202 : function(resposta) {
					funcoes.atencao(resposta);
				},				
				422 : function(resposta) {
					funcoes.erro(resposta);
				}
			};
		},
		
		/**
		 * abre uma pagina
		 * @param url
		 */
		abrir : function(url) {
			app.paginaDestino = url;
			var fnSucesso = function(resultado) {
				$("#conteudo").html(resultado);
				scriptUtils.mudarScripts(app.paginaAtual, app.paginaDestino);
				app.paginaAtual = url;
			};
			var fnErro = function(resultado) {
				app.paginaDestino = "";
				$("#conteudo").html(resultado.responseText);
			};
			
			app.Ajax(url, app.metodoGET, app.assincrono, fnSucesso, fnErro);
		},
		
		getJSON : function(url, metodo, assincrono, funcoes, data, crossDomain) {
			if (typeof crossDomain == undefined) {
				crossDomain = false;
			}
			$.ajax({
				url 		: url,
				method 		: metodo,
				async 		: assincrono,
//				dataType: "text/html",
//				accepts 	: 'application/json',
//				contentType : 'charset=UTF-8',
				data 		: data,
				crossDomain : crossDomain,
				statusCode : all.getTratamentosAjax(funcoes)
			});
		},
		
		converterTextoEmJSON : function(texto) {
			var arrayDeObjetos;
			try {
				arrayDeObjetos = $.parseJSON(texto);
			} catch (e) {
				arrayDeObjetos = texto;
			}
			return arrayDeObjetos;
		},
		
		formatarLinks : function(componenteAlvo) {
			debugger;
			var links = typeof componenteAlvo === undefined ? $("a") : $(componenteAlvo).find("a");   
			links.on('click', function(e) {
				var url = $(this).attr('href');
				var funcoes = {
					sucesso : function(retorno) {
						all.trocarConteudo(retorno);
					}	
				};
				all.Ajax(url, all.metodoGET, all.sincrono, funcoes, all.fnFalhaPadrao);
				e.preventDefault();
			});
		},
		
		fnFalhaPadrao : function(resultado) {
			console.log(retorno);	
		},
		
		trocarConteudo : function(conteudo) {
			$("#conteudo").html('');
			$("#conteudo").html(conteudo);
		}
};

var JQuery = 'http://code.jquery.com/jquery-2.1.4.min.js';
var JQueryUi = 'http://code.jquery.com/ui/1.11.4/jquery-ui.js';
var tagHead = document.getElementsByTagName('head')[0].innerHTML;
var ajaxAsyncPadrao = true;

function existe(o) {
	return o != null;
};

var naoExiste = function(o) {
	return !existe(o) || (typeof o === 'undefined');
};

var jaTemScript = function(fonte) {
	var tagHead = document.getElementsByTagName('head')[0];
	var tagsScript = tagHead.getElementsByTagName("script");
	
	for (var indice = 0; indice < tagsScript.length; indice++) {
		var tem = tagsScript[indice].src.indexOf(fonte) >= 0;
		if (tem) {
			return true;
		}
	}
	return false;
};

var importacaoJQuery = function(){
	if (!jaTemScript(JQuery)) {
		var tagScript = document.createElement('script');
		tagScript.setAttribute('src', JQuery);
		document.head.appendChild(tagScript);
	}
};
//importacaoJQuery();

var importacaoJQueryUi = function(){
	if (!jaTemScript(JQueryUi)) {
		var tagScript = document.createElement('script');
		tagScript.setAttribute('src', JQueryUi);
		document.head.appendChild(tagScript);
	}
};

/*all.scripts = [
    {
    	src : JQuery,
    	load : function() {
    		importacaoJQueryUi();
    	}
	},
	{
		src : JQueryUi,
		load : function() {
			
		}
	},
	
];
*/

var init = function(executar) {
	if (existe(executar) && typeof executar === 'function') {
		executar();
	}
//    all.formatarLinks();
};

function initMenu() {
	$( "#menu" ).menu();
    $( "#menu" ).removeClass("oculto");
}
//init(importacaoJQueryUi());


/*all.CONSEQUENCIAS = {
		SEM_ACESSO : 				'SEM_ACESSO',
		DESLOGADO_COM_SUCESSO : 	'DESLOGADO_COM_SUCESSO',
		ERRO : 						'ERRO',
		SUCCESSO : 					'SUCCESSO',
		MUITOS_ERROS : 				'MUITOS_ERROS',
		ATENCAO : 					'ATENCAO',
		TENTE_NOVAMENTE : 			'TENTE_NOVAMENTE',
		PARE_DE_TENTAR_NOVAMENTE =  'PARE_DE_TENTAR_NOVAMENTE'
};*/


getValorValido = function(valor) {
	if (naoExiste(valor)) {
		return '';
	}
	return valor;
};

function log(texto) {
	console.log(texto);
};

function exibir(o) {
	if (existe(o)) {
		o.removeClass("oculto");
	}
};

function ocultar(o) {
	if (existe(o)) {
		o.addClass("oculto");
	}
};