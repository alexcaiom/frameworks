package br.com.massuda.alexander.persistencia.interfaces;

import java.sql.SQLException;
import java.util.List;

public interface IFinder<T> {

	public T pesquisar(Long id);
	public List<T> listar();
	
	public void preencher(T o) throws SQLException;
	
}
