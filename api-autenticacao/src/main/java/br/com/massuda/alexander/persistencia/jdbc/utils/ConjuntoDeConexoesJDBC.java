package br.com.massuda.alexander.persistencia.jdbc.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.waiso.framework.abstratas.Classe;

public class ConjuntoDeConexoesJDBC extends Classe {
	
	private static ConjuntoDeConexoesJDBC instancia = null;
	protected static List<Connection> conexoesDisponiveis = new ArrayList<Connection>();
	protected static List<Connection> conexoesEmUso = new ArrayList<Connection>();
	
	private ConjuntoDeConexoesJDBC() {
		int conexoesASeremCriadas = ConstantesPersistencia.QUANTIDADE_MAXIMA_DE_CONEXOES;
		for (int i = 0; i < conexoesASeremCriadas; i++) {
			try {
				Connection conexao = GerenciadorConexaoJDBC.novaConexao();
				conexoesDisponiveis.add(conexao);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Connection getConexaoDisponivel() throws SQLException {
		if (naoExiste(instancia)) {
			instancia = new ConjuntoDeConexoesJDBC();
		}
		for (Connection conexao : conexoesDisponiveis) {
			if (existe(conexao) && !conexao.isClosed()) {
				conexoesDisponiveis.remove(conexao);
				conexoesEmUso.add(conexao);
				return conexao;
			}
		}
		return null;
	}
	
	public static void liberarConexao(Connection conexao) {
		if (existe(conexao)) {
			for (Connection con : conexoesEmUso) {
				if (con == conexao) {
					conexoesDisponiveis.add(con);
					conexoesEmUso.remove(conexao);
				}
			}
		}
	}
	
	public void finalizar() {
		try {
			if (conexoesDisponiveis.isEmpty() && !conexoesEmUso.isEmpty()) {
				for (Connection conexao : conexoesEmUso) {
					liberarConexao(conexao);
				}
			}

			for (Connection conexao : conexoesDisponiveis) {
				if (existe(conexao) && !conexao.isClosed()) {
					conexao.close();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
