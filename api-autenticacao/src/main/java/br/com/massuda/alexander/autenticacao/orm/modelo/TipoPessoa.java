package br.com.massuda.alexander.autenticacao.orm.modelo;

public enum TipoPessoa {

	PESSOA_FISICA(1l),
	PESSOA_JURIDICA(2l);
	
	Long id;
	
	private TipoPessoa(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
	
	public static TipoPessoa getPorCodigo (long codigo) {
		for (TipoPessoa tipo : values()) {
			if (tipo.id == codigo) {
				return tipo;
			}
		}
		return null;
	}
	
}
