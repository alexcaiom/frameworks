package br.com.massuda.alexander.persistencia.jdbc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.massuda.alexander.persistencia.jdbc.utils.GerenciadorConexaoJDBC;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.waiso.framework.abstratas.Classe;

public abstract class DAOJDBC extends Classe{
	
	public Statement comando = null;
	public PreparedStatement comandoPreparado = null;
	public ResultSet resultados = null;
	public static final Integer RETORNAR_ID_GERADO = Statement.RETURN_GENERATED_KEYS;
	public OperacaoCRUD tipoCRUD = null;
	
	public TipoOperacao tipoOperacao = TipoOperacao.NORMAL;
	
	public void executarComando(String sql) throws SQLException{
		setComando(getConexao().createStatement());
		getComando().execute(sql);
		fechar();
	}
	
	public ResultSet pesquisarSemParametro(String sql) throws SQLException{
		setComando(getConexao().createStatement());
		setResultados(getComando().executeQuery(sql));
		return getResultados();
	}
	
	public boolean executarOperacaoParametrizada(PreparedStatement comandoPreparado) throws SQLException {
		return comandoPreparado.execute();
	}
	
	public ResultSet pesquisarComParametros(PreparedStatement comandoPreparado) throws SQLException {
		return comandoPreparado.executeQuery();
	}
	
	public ResultSet executarOperacaoERetornarChavesGeradas(PreparedStatement operacao) throws SQLException {
		operacao.execute();
		return operacao.getGeneratedKeys();
	}
	
	public Connection getConexao() throws SQLException {
		
		return GerenciadorConexaoJDBC.getConexao();
	}
	
	public Statement novoComando() throws SQLException {
		return getConexao().createStatement();
	}
	
	public PreparedStatement novoComandoPreparado(String sql, Integer parametro) throws SQLException{
		if (existe(parametro)) {
			return getConexao().prepareStatement(sql, parametro);
		} else {
			return getConexao().prepareStatement(sql);
		}
	}
	
	public PreparedStatement novoComandoPreparado(String sql) throws SQLException{
		return novoComandoPreparado(sql, null);
	}
	
	public Statement getComando() {
		return comando;
	}
	public void setComando(Statement comando) {
		this.comando = comando;
	}
	public PreparedStatement getComandoPreparado() {
		return comandoPreparado;
	}
	public void setComandoPreparado(PreparedStatement comandoPreparado) {
		this.comandoPreparado = comandoPreparado;
	}
	public ResultSet getResultados() {
		return resultados;
	}
	public void setResultados(ResultSet resultados) {
		this.resultados = resultados;
	}
	
	public void fechar() throws SQLException {
		if (tipoOperacao == TipoOperacao.NORMAL) {
			fecharObjetosDeComandoEPesquisa(comando, comandoPreparado, resultados);
			/**
			 * FIXME Teste de fechamento de Conexao na Classe Controladora
			 */
			GerenciadorConexaoJDBC.fecharConexao();
		}
	}

	public void fecharObjetosDeComandoEPesquisa(Statement comando, PreparedStatement comandoPreparado, ResultSet resultados) throws SQLException {
		if (existe(comando) && !comando.isClosed()) {
			comando.close();
			setComando(null);
		}
		if (existe(comandoPreparado) && !comandoPreparado.isClosed()) {
			comandoPreparado.close();
			setComandoPreparado(null);
		}
		if (existe(resultados) && !resultados.isClosed()) {
			resultados.close();
			setResultados(null);
		}
	}
}