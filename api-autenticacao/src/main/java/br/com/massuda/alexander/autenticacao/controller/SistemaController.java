package br.com.massuda.alexander.autenticacao.controller;

import java.util.List;

import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.massuda.alexander.autenticacao.controller.abstratas.ControllerSpring;
import br.com.massuda.alexander.autenticacao.controller.utils.RespostaAjax;
import br.com.massuda.alexander.autenticacao.dao.SistemaDAO;
import br.com.massuda.alexander.autenticacao.dao.finder.FinderSistema;
import br.com.massuda.alexander.autenticacao.excecoes.Erro;
import br.com.massuda.alexander.autenticacao.orm.modelo.sistema.Sistema;

@Controller
@RequestMapping("/sistema")
public class SistemaController extends ControllerSpring {

	@Autowired
	private FinderSistema finder;
	@Autowired
	private SistemaDAO dao;
	
	@ResponseStatus(code=HttpStatus.OK)
	@RequestMapping(method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public RespostaAjax<? extends Object> incluir (@RequestBody Sistema sistema) {
		try {
			log(SistemaController.class, sistema.toString());
			sistema = dao.incluir(sistema);
		} catch (Erro e){
			return new RespostaAjax<Erro>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new RespostaAjax<Object>(HttpStatus.OK);
	}
	
	@ResponseBody
	@RequestMapping
	public List<Sistema> listar () {
		List<Sistema> sistemas = finder.listar();
		getLogger(getClass()).debug(sistemas);
		return sistemas;
	}
	
	@ResponseBody
	@RequestMapping("/pesquisarPorNome/{nome}")
	public List<Sistema> pesquisarPorNomeComo (@PathVariable("nome") String nome) {
		List<Sistema> sistemas = finder.pesquisarPorNomeComo(nome);
		return sistemas;
	}
	
	@ResponseBody
	@RequestMapping("/pesquisarPorId/{id}")
	public Sistema pesquisar (@PathVariable("id") Long id) {
		Sistema sistema = finder.pesquisar(id);
		return sistema;
	}

	@ResponseStatus(code=HttpStatus.OK)
	@RequestMapping(method=RequestMethod.DELETE)
	public void excluir (@RequestParam long id) {
		Sistema o = finder.pesquisar(id);
		dao.excluir(o);
	}
	
}
