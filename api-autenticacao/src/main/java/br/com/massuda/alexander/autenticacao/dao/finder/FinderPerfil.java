package br.com.massuda.alexander.autenticacao.dao.finder;

import br.com.massuda.alexander.autenticacao.orm.modelo.Perfil;
import br.com.massuda.alexander.persistencia.interfaces.IFinder;

public interface FinderPerfil extends IFinder<Perfil> {

}
