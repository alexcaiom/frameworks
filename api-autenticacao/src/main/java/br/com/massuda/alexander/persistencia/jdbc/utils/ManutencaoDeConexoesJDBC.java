package br.com.massuda.alexander.persistencia.jdbc.utils;

import java.sql.Connection;
import java.sql.SQLException;

public class ManutencaoDeConexoesJDBC extends Thread {

	private long tempoDeSincronizacaoEmSegundos = 0;
	private boolean continuar = true;
	private ConjuntoDeConexoesJDBC conjunto = null;
	
	public ManutencaoDeConexoesJDBC(ConjuntoDeConexoesJDBC conjunto) {
		tempoDeSincronizacaoEmSegundos = ConstantesPersistencia.TEMPO_SINCRONIZACAO_EM_SEGUNDOS;
		this.conjunto = conjunto;
	}
	
	@Override
	public void run() {
		while (continuar) {
			try {
				synchronized (ConjuntoDeConexoesJDBC.conexoesDisponiveis) {
					for (Connection conexao : ConjuntoDeConexoesJDBC.conexoesDisponiveis) {
						boolean conexaoValida = conexao.isValid(0);
						boolean conexaoFechada = conexao.isClosed();
						if (!conexaoValida) {
							if (conexao.isClosed()) {
								conexao = GerenciadorConexaoJDBC.novaConexao();
							}
						}
					}
				}
				sleep(tempoDeSincronizacaoEmSegundos);
			} catch (InterruptedException e) {
				encerrar();
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		encerrarConexoes();
	}
	
	private void encerrarConexoes() {
		conjunto.finalizar();
	}

	public void encerrar() {
		continuar = false;
		encerrarConexoes();
	}
	
}
