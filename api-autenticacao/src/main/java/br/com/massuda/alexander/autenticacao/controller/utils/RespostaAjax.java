package br.com.massuda.alexander.autenticacao.controller.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class RespostaAjax<T> extends ResponseEntity<T> {
	
	public RespostaAjax(HttpStatus statusCode) {
		super(statusCode);
	}

	public RespostaAjax(T body, HttpStatus statusCode) {
		super(body, statusCode);
	}
}
