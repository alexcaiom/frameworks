package br.com.massuda.alexander.autenticacao.dao.finder;

import java.util.List;

import br.com.massuda.alexander.autenticacao.excecoes.Erro;
import br.com.massuda.alexander.autenticacao.orm.modelo.sistema.Sistema;
import br.com.massuda.alexander.persistencia.interfaces.IFinder;

public interface FinderSistema extends IFinder<Sistema> {
	public List<Sistema> pesquisarPorNomeComo (String nome) throws Erro;
}
