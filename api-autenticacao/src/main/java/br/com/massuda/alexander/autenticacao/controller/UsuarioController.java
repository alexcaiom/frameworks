package br.com.massuda.alexander.autenticacao.controller;

import java.util.GregorianCalendar;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.massuda.alexander.autenticacao.bo.BOUsuario;
import br.com.massuda.alexander.autenticacao.controller.abstratas.ControllerSpring;
import br.com.massuda.alexander.autenticacao.controller.utils.RespostaAjax;
import br.com.massuda.alexander.autenticacao.dao.finder.FinderPerfil;
import br.com.massuda.alexander.autenticacao.excecoes.Erro;
import br.com.massuda.alexander.autenticacao.excecoes.ErroNegocio;
import br.com.massuda.alexander.autenticacao.orm.modelo.Foto;
import br.com.massuda.alexander.autenticacao.orm.modelo.usuario.Usuario;
import br.com.massuda.alexander.autenticacao.utils.Constantes;
import br.com.massuda.alexander.autenticacao.utils.SessaoUsuario;
import br.com.massuda.alexander.autenticacao.utils.SessoesUsuario;
import br.com.waiso.framework.abstratas.Classe;

@Controller
@RequestMapping("/usuario")
public class UsuarioController extends ControllerSpring {
	
//	private int maxFileSize = 50 * 1024;
//	private int maxMemSize = 4* 1024;

	
	@Autowired
	private BOUsuario boUsuario;
	@Autowired
	private FinderPerfil finderPerfil;

	@ResponseBody
	@RequestMapping(method=RequestMethod.POST)
	public void incluir(@RequestBody @Valid Usuario usuario/*, @RequestParam(required=false) MultipartFile[] arquivos*/) {
		aplicarPadraoUsuario(usuario);
		List<Foto> fotos = null;
		String message = "";
		
		/*if (arquivos != null && arquivos.length > 0) {
			fotos = new ArrayList<Foto>();
			for (int i = 0; i < arquivos.length; i++) {
				MultipartFile arquivo = arquivos[i];
				String name = arquivo.getName();
				try {
					byte[] bytes = arquivo.getBytes();

					// Creating the directory to store file
					String rootPath = System.getProperty("catalina.home");
					File dir = new File(rootPath + File.separator + "tmpFiles");
					if (!dir.exists())
						dir.mkdirs();

					// Create the file on server
					File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(bytes);
					stream.close();

					Foto foto = new Foto();
					foto.setCaminho(serverFile.getAbsolutePath());
					foto.setData(GregorianCalendar.getInstance());
					foto.setDescricao(arquivo.getName());
					fotos.add(foto);

					//				logger.info("Server File Location="+ serverFile.getAbsolutePath());


					message = message + "You successfully uploaded file=" + name + "";
				} catch (Exception e) {
					message = message + "mensagem: You failed to upload " + name + " => " + e.getMessage();
				}
			}
		}*/
		
		usuario = boUsuario.incluir(usuario);
	}

	/**
	 * @param usuario
	 */
	private void aplicarPadraoUsuario(Usuario usuario) {
		usuario.setPerfil(finderPerfil.pesquisar(2l));
	}
	
	@RequestMapping(value="/autenticar", method=RequestMethod.POST)
	public RespostaAjax<? extends Object> autenticar(String login, String senha) {
		RespostaAjax<? extends Object> resposta = null;
		
		Usuario usuario = null;
		try {
			usuario = boUsuario.autentica(login, senha);
			adicionarSessaoDeUsuario(usuario);
			resposta = new RespostaAjax<Object>(HttpStatus.OK);
		} catch (ErroNegocio e) {
			String mensagemDeErro = e.getMensagem();
			resposta = new RespostaAjax<String>(mensagemDeErro, HttpStatus.ACCEPTED);
		} catch (Erro e) {
			String mensagemDeErro = e.getMensagem();
			resposta = new RespostaAjax<String>(mensagemDeErro, HttpStatus.UNPROCESSABLE_ENTITY);
		}
		
		return resposta;
	}
	
	@RequestMapping(value="/verificaSessao", method=RequestMethod.POST)
	public RespostaAjax<String> usuarioEstaLogado(String token) {
		RespostaAjax<String> resposta = null;
		try {
			SessoesUsuario sessoes = getSessoesUsuario();
			boolean usuarioLogado = sessoes.tem(token);
			if (usuarioLogado) {
				boolean valida = sessoes.valida(token);
				if (valida) {
					resposta = new RespostaAjax<String>(HttpStatus.OK);
				} else {
					String mensagem = "Sessão expirada";
					resposta = new RespostaAjax<String>(mensagem, HttpStatus.ACCEPTED);
				}
			}
		} catch (ErroNegocio e) {
			String mensagemDeErro = e.getMensagem();
			resposta = new RespostaAjax<String>(mensagemDeErro, HttpStatus.ACCEPTED);
		} catch (Erro e) {
			String mensagemDeErro = e.getMensagem();
			resposta = new RespostaAjax<String>(mensagemDeErro, HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return resposta;
	}
	
	private SessoesUsuario adicionarSessaoDeUsuario(Usuario usuario) {
		SessoesUsuario sessoes = getSessoesUsuario();
		
		SessaoUsuario sessaoDeUsuario = new SessaoUsuario(usuario.getLogin(), usuario, GregorianCalendar.getInstance());
		sessoes.add(usuario.getLogin(), sessaoDeUsuario);
		return sessoes;
	}

	private SessoesUsuario getSessoesUsuario() {
		SessoesUsuario sessoes = (SessoesUsuario) sessao.getAttribute(Constantes.USUARIO);
		
		if (Classe.naoExiste(sessoes)) {
			sessoes = new SessoesUsuario();
			sessao.setAttribute(Constantes.USUARIO, sessoes);
		}
		return sessoes;
	}

	@ResponseBody
	@RequestMapping
	public List<Usuario> listar() {
		List<Usuario> usuarios = boUsuario.listarUsuarios();
		return usuarios; 
	}
	
	@RequestMapping("/pesquisarPorNome/{nome}")
	public ResponseEntity<List<Usuario>> pesquisarPorNome(String nome) {
		List<Usuario> usuarios = boUsuario.pesquisarPorNome(nome);
		return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.OK); 
	}
	
}
