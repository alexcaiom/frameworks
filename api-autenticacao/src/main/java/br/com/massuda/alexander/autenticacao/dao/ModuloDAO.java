package br.com.massuda.alexander.autenticacao.dao;

import br.com.massuda.alexander.autenticacao.orm.modelo.sistema.Modulo;
import br.com.massuda.alexander.persistencia.interfaces.IDAO;

public interface ModuloDAO extends IDAO<Modulo> {

}
