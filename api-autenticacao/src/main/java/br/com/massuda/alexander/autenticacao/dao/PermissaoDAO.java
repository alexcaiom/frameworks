package br.com.massuda.alexander.autenticacao.dao;

import br.com.massuda.alexander.autenticacao.orm.modelo.Permissao;
import br.com.massuda.alexander.persistencia.interfaces.IDAO;

public interface PermissaoDAO extends IDAO<Permissao> {

}
