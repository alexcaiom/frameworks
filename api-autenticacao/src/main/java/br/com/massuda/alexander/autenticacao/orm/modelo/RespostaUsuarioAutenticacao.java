package br.com.massuda.alexander.autenticacao.orm.modelo;
/**
 * @author Alex
 *
 */
public enum RespostaUsuarioAutenticacao {

	SUCESSO									(0l, "Te encontramos!"),
	USUARIO_INEXISTENTE						(1l, "Usuário inexistente"),
	SENHA_INVALIDA							(2l, "Senha inválida"),
	SENHA_INVALIDA_ULTIMA_TENTATIVA			(3l, "Senha inválida.\n Proximo erro ocasionara bloqueio da conta."),
	USUARIO_BLOQUEADO						(4l, "Usuário bloqueado"),
	FINAL_DE_SEMANA							(5l, "O acesso é restrito aos dias de semana!"),
	FORA_DO_HORARIO_COMERCIAL				(6l, "O sistema só pode ser acessado no horário comercial.");
	
	Long id;
	String mensagem;
	
	private RespostaUsuarioAutenticacao(long id, String mensagem) {
		this.id = id;
		this.mensagem = mensagem;
	}

	/**
	 * @return the cod
	 */
	public final Long getId() {
		return id;
	}

	/**
	 * @return the mensagem
	 */
	public final String getMensagem() {
		return mensagem;
	}	
	
	public static RespostaUsuarioAutenticacao get(int id){
		for (RespostaUsuarioAutenticacao status : RespostaUsuarioAutenticacao.values()) {
			if (status.getId() == id) {
				return status;
			}
		}
		return null;
	}
}