/**
 * 
 */
package br.com.waiso.framework.configuracao;

import java.util.Set;

/**
 * @author Alex
 *
 */
public interface ConfigurationUtilsStrategy {

	public abstract Set<String> getPropertyNames();
	public abstract String getProperty(String property);
	public abstract String getProperty(String property, String valorDefault);
	public abstract void setProperty(String property, String valorDefault);
	
}
