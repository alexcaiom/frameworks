/**
 * 
 */
package br.com.waiso.framework.configuracao;

import java.util.Set;

/**
 * @author Alex
 *
 */
public interface ConfigurationRepository {

	public abstract String getProperty(String s);
	public abstract boolean isReadOnly();
	public abstract void setProperty(String s, String padrao);
	public abstract boolean hasPropertyNames();
	public abstract Set<String> getPropertyNames();
	
}
