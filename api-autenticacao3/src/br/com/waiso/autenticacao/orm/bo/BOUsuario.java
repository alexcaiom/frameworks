package br.com.waiso.autenticacao.orm.bo;

import java.util.List;

import br.com.waiso.autenticacao.excecoes.Erro;
import br.com.waiso.autenticacao.orm.modelo.usuario.Usuario;

public interface BOUsuario extends IBO<Usuario> {

	public Usuario autentica(String login, String senha) throws Erro;
	public List<Usuario> listarUsuarios() throws Erro;
	public Usuario incluir(Usuario u);
	public List<Usuario> pesquisarPorNome(String nome);
	
}
