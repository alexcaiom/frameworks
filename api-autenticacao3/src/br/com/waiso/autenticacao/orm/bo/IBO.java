package br.com.waiso.autenticacao.orm.bo;

import br.com.waiso.autenticacao.excecoes.Erro;

/**
 * @author Alex
 *
 */
public interface IBO<T> {

	/**
	 * Metodos de Exclusao sao preparados para realizar exclusao de Listas<br/>
	 * de Objetos
	 * @param o
	 * @throws Erro
	 */
	public abstract void excluir(T o) throws Erro;
	
}