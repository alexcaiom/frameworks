package br.com.waiso.autenticacao.orm.dao.impl;

import br.com.waiso.autenticacao.utils.Constantes;
import br.com.waiso.persistencia.jdbc.DAOGenericoJDBCImpl;
import br.com.waiso.persistencia.jdbc.utils.ConstantesPersistencia;

public abstract class DAO<T> extends DAOGenericoJDBCImpl<T> {

	public DAO(Class<T> entidade) {
		super(entidade);
		configurarBancoDeDadosDadosAcesso();
	}

	/**
	 * 
	 */
	public static void configurarBancoDeDadosDadosAcesso() {
		ConstantesPersistencia.BD_CONEXAO_LOCAL = Constantes.BANCO_DE_DADOS_LOCAL;
		ConstantesPersistencia.BD_CONEXAO_USUARIO = Constantes.BANCO_DE_DADOS_CONEXAO_USUARIO;
		ConstantesPersistencia.BD_CONEXAO_SENHA = Constantes.BANCO_DE_DADOS_CONEXAO_SENHA;
		ConstantesPersistencia.BD_CONEXAO_NOME_BD = Constantes.BANCO_DE_DADOS_NOME;
	}
	
}
