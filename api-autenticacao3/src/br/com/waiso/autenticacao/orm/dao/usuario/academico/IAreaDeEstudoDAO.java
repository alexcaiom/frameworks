package br.com.waiso.autenticacao.orm.dao.usuario.academico;

import br.com.waiso.autenticacao.orm.modelo.usuario.academico.AreaDeEstudo;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IAreaDeEstudoDAO extends IDAO<AreaDeEstudo> {

}
