package br.com.waiso.autenticacao.orm.dao.impl.usuario.profissional;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.autenticacao.orm.dao.usuario.profissional.IExperienciaProfissionalDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.ExperienciaProfissional;
import br.com.waiso.framework.exceptions.ErroUsuario;

public class ExperienciaProfissionalDAOImpl extends DAO<ExperienciaProfissional> implements IExperienciaProfissionalDAO {

	public ExperienciaProfissionalDAOImpl() {
		super(ExperienciaProfissional.class);
	}

	@Override
	public void editar(ExperienciaProfissional o) throws ErroUsuario {
		super.editar(o);
	}

}
