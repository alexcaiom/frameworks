package br.com.waiso.autenticacao.orm.dao;

import br.com.waiso.autenticacao.orm.modelo.sistema.Sistema;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface ISistemaDAO extends IDAO<Sistema> {

}
