package br.com.waiso.autenticacao.orm.dao.impl.usuario.profissional;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.autenticacao.orm.dao.usuario.profissional.IEmpresaDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Empresa;

public class EmpresaDAOImpl extends DAO<Empresa> implements IEmpresaDAO {

	public EmpresaDAOImpl() {
		super(Empresa.class);
	}

}
