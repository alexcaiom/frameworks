package br.com.waiso.autenticacao.orm.dao.impl.usuario.profissional;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.autenticacao.orm.dao.usuario.profissional.ICargoDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Cargo;

public class CargoDAOImpl extends DAO<Cargo> implements ICargoDAO {

	public CargoDAOImpl() {
		super(Cargo.class);
	}

}
