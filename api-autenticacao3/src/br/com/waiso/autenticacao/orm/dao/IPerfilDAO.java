package br.com.waiso.autenticacao.orm.dao;

import br.com.waiso.autenticacao.orm.modelo.Perfil;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IPerfilDAO extends IDAO<Perfil> {

}
