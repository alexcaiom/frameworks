package br.com.waiso.autenticacao.orm.dao.finder;

import br.com.waiso.autenticacao.orm.modelo.sistema.Sistema;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderSistema extends IFinder<Long, Sistema> {
	
}
