package br.com.waiso.autenticacao.orm.dao.usuario.profissional;

import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Empresa;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IEmpresaDAO extends IDAO<Empresa> {

}
