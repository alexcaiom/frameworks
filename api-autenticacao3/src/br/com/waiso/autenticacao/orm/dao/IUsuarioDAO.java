package br.com.waiso.autenticacao.orm.dao;

import br.com.waiso.autenticacao.orm.modelo.usuario.Usuario;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IUsuarioDAO extends IDAO<Usuario> {

}
