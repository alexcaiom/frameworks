package br.com.waiso.autenticacao.orm.dao.impl;

import br.com.waiso.autenticacao.orm.dao.IPerfilDAO;
import br.com.waiso.autenticacao.orm.modelo.Perfil;
import br.com.waiso.framework.exceptions.ErroUsuario;

public class PerfilDAOImpl extends DAO<Perfil> implements IPerfilDAO {

	public PerfilDAOImpl() {
		super(Perfil.class);
	}

	@Override
	public Perfil incluir(Perfil o) throws ErroUsuario {
		return super.incluir(o);
	}

	@Override
	public void editar(Perfil o) throws ErroUsuario {
		super.editar(o);
	}

	@Override
	public void excluir(Perfil o) throws ErroUsuario {
		super.excluir(o);
	}

}
