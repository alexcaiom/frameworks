package br.com.waiso.autenticacao.orm.dao.usuario.profissional;

import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.ExperienciaProfissional;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IExperienciaProfissionalDAO extends IDAO<ExperienciaProfissional> {

}
