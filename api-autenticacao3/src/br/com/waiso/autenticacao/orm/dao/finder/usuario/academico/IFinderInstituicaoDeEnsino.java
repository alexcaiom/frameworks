package br.com.waiso.autenticacao.orm.dao.finder.usuario.academico;

import br.com.waiso.autenticacao.orm.modelo.usuario.academico.InstituicaoDeEnsino;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderInstituicaoDeEnsino extends IFinder<Long, InstituicaoDeEnsino> {
	
}
