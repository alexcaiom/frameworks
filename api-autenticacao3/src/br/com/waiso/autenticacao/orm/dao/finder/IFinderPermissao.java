package br.com.waiso.autenticacao.orm.dao.finder;

import br.com.waiso.autenticacao.orm.modelo.Permissao;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderPermissao extends IFinder<Long, Permissao> {
	
}
