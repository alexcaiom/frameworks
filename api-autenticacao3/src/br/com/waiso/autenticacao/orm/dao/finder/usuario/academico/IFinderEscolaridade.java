package br.com.waiso.autenticacao.orm.dao.finder.usuario.academico;

import br.com.waiso.autenticacao.orm.modelo.usuario.academico.Escolaridade;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderEscolaridade extends IFinder<Long, Escolaridade> {
	
}
