package br.com.waiso.autenticacao.orm.dao.finder;

import br.com.waiso.autenticacao.orm.modelo.Perfil;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderPerfil extends IFinder<Long, Perfil> {

}
