package br.com.waiso.autenticacao.orm.dao;

import br.com.waiso.autenticacao.orm.modelo.Permissao;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IPermissaoDAO extends IDAO<Permissao> {

}
