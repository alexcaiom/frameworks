package br.com.waiso.autenticacao.orm.dao;

import br.com.waiso.autenticacao.orm.modelo.sistema.Funcionalidade;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IFuncionalidadeDAO extends IDAO<Funcionalidade> {

}
