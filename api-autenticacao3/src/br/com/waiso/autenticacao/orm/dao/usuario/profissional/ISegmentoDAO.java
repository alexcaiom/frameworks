package br.com.waiso.autenticacao.orm.dao.usuario.profissional;

import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Segmento;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface ISegmentoDAO extends IDAO<Segmento> {

}
