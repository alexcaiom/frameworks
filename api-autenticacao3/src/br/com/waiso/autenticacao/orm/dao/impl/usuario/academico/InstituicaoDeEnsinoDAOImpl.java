package br.com.waiso.autenticacao.orm.dao.impl.usuario.academico;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.autenticacao.orm.dao.usuario.academico.IInstituicaoDeEnsinoDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.academico.InstituicaoDeEnsino;

public class InstituicaoDeEnsinoDAOImpl extends DAO<InstituicaoDeEnsino> implements IInstituicaoDeEnsinoDAO {

	public InstituicaoDeEnsinoDAOImpl() {
		super(InstituicaoDeEnsino.class);
	}

}
