package br.com.waiso.autenticacao.orm.dao.impl.usuario.academico;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.autenticacao.orm.dao.usuario.academico.IAtividadeEscolarDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.academico.AtividadeEscolar;

public class AtividadeEscolarDAOImpl extends DAO<AtividadeEscolar> implements IAtividadeEscolarDAO {

	public AtividadeEscolarDAOImpl() {
		super(AtividadeEscolar.class);
	}

}
