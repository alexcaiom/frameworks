package br.com.waiso.autenticacao.orm.dao.impl.usuario.profissional;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.autenticacao.orm.dao.usuario.profissional.ISegmentoDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Segmento;

public class SegmentoDAOImpl extends DAO<Segmento> implements ISegmentoDAO {

	public SegmentoDAOImpl() {
		super(Segmento.class);
	}

}
