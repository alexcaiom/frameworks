package br.com.waiso.autenticacao.orm.dao;

import br.com.waiso.autenticacao.orm.modelo.sistema.Modulo;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IModuloDAO extends IDAO<Modulo> {

}
