package br.com.waiso.autenticacao.orm.dao.impl.usuario.academico;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.autenticacao.orm.dao.usuario.academico.IEscolaridadeDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.academico.Escolaridade;

public class EscolaridadeDAOImpl extends DAO<Escolaridade> implements IEscolaridadeDAO {

	public EscolaridadeDAOImpl() {
		super(Escolaridade.class);
	}

}
