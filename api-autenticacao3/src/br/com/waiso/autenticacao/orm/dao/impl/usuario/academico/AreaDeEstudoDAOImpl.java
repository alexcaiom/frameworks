package br.com.waiso.autenticacao.orm.dao.impl.usuario.academico;

import br.com.waiso.autenticacao.orm.dao.impl.DAO;
import br.com.waiso.autenticacao.orm.dao.usuario.academico.IAreaDeEstudoDAO;
import br.com.waiso.autenticacao.orm.modelo.usuario.academico.AreaDeEstudo;

public class AreaDeEstudoDAOImpl extends DAO<AreaDeEstudo> implements IAreaDeEstudoDAO {

	public AreaDeEstudoDAOImpl() {
		super(AreaDeEstudo.class);
	}

}
