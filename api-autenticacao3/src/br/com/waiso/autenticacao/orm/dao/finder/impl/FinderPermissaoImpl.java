package br.com.waiso.autenticacao.orm.dao.finder.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.waiso.autenticacao.orm.dao.finder.IFinderPermissao;
import br.com.waiso.autenticacao.orm.dao.finder.impl.usuario.FinderUsuarioImpl;
import br.com.waiso.autenticacao.orm.modelo.Permissao;
import br.com.waiso.autenticacao.orm.modelo.sistema.Funcionalidade;
import br.com.waiso.autenticacao.orm.modelo.usuario.Usuario;
import br.com.waiso.persistencia.jdbc.GeradorSQLBean;
import br.com.waiso.persistencia.jdbc.utils.TipoOperacao;

public class FinderPermissaoImpl extends Finder<Permissao> implements IFinderPermissao {
	
	public FinderPermissaoImpl() {
		super(Permissao.class);
		tipoOperacao = TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS;
	}
	
	public Permissao pesquisarPorUsuario(Long id) {
		Permissao p = new Permissao();
		String sql = GeradorSQLBean.getInstancia(Permissao.class).getComandoSelecao();
		
		sql += "\n where usuario_id = ?";
		
		try {
			comandoPreparado = novoComandoPreparado(sql);
			comandoPreparado.setLong(1, id);
			
			resultados = pesquisarComParametros(comandoPreparado);
			
			if (resultados.next()) {
				preencher(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return p;
	}
	
	public Permissao pesquisar(Long id) {
		Permissao p = new Permissao();
		String sql = GeradorSQLBean.getInstancia(Permissao.class).getComandoSelecao();
		
		sql += "\n where id = ?";
		
		try {
			comandoPreparado = novoComandoPreparado(sql);
			comandoPreparado.setLong(1, id);
			
			resultados = pesquisarComParametros(comandoPreparado);
			
			if (resultados.next()) {
				preencher(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return p;
	}

	public List<Permissao> listar() {
		List<Permissao> permissoes = new ArrayList<Permissao>();
		String sql = GeradorSQLBean.getInstancia(Permissao.class).getComandoSelecao();
		try {
			resultados = pesquisarSemParametro(sql);
			
			while (resultados.next()) {
				Permissao p = new Permissao();
				preencher(p);
				permissoes.add(p);
			}
			
			tipoOperacao = TipoOperacao.NORMAL;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				fechar();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return permissoes;
	}

	public void preencher(Permissao o) throws SQLException {
		if (existe(o) && existe(resultados) && !resultados.isClosed()) {
			super.preencher(o);
			tipoOperacao = TipoOperacao.PESQUISA_COM_MULTIPLAS_TABELAS;

			Long idUsuario = o.getUsuario().getId();
			Usuario usuarioAssociado = new FinderUsuarioImpl(tipoOperacao).pesquisar(idUsuario);
			
			Long idFuncionalidade = o.getFuncionalidade().getId();
			Funcionalidade funcionalidadeAssociada = new FinderFuncionalidadeImpl(tipoOperacao).pesquisar(idFuncionalidade);
			
			o.setUsuario(usuarioAssociado);
			o.setFuncionalidade(funcionalidadeAssociada);
		}
	}

}
