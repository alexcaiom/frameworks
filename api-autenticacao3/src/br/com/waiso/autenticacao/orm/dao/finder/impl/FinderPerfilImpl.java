package br.com.waiso.autenticacao.orm.dao.finder.impl;

import java.sql.SQLException;
import java.util.List;

import br.com.waiso.autenticacao.orm.modelo.Perfil;
import br.com.waiso.persistencia.interfaces.IFinder;
import br.com.waiso.persistencia.jdbc.utils.TipoOperacao;

public class FinderPerfilImpl extends Finder<Perfil> implements IFinder<Long, Perfil> {

	public FinderPerfilImpl() {
		super(Perfil.class);
		tipoOperacao = TipoOperacao.NORMAL;
	}
	
	public FinderPerfilImpl(TipoOperacao tipoOperacao) {
		super(Perfil.class);
		this.tipoOperacao = tipoOperacao;
	}

	@Override
	public Perfil pesquisar(Long id) {
		return super.pesquisar(id);
	}

	@Override
	public List<Perfil> listar() {
		return super.listar();
	}
	
	public void preencher(Perfil o) throws SQLException {
		super.preencher(o);
	}

}
