package br.com.waiso.autenticacao.orm.dao.usuario.academico;

import br.com.waiso.autenticacao.orm.modelo.usuario.academico.Escolaridade;
import br.com.waiso.persistencia.interfaces.IDAO;

public interface IEscolaridadeDAO extends IDAO<Escolaridade> {

}
