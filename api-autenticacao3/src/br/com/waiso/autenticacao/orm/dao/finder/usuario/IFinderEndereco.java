package br.com.waiso.autenticacao.orm.dao.finder.usuario;

import br.com.waiso.autenticacao.orm.modelo.Endereco;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderEndereco extends IFinder<Long, Endereco> {
	
	
}
