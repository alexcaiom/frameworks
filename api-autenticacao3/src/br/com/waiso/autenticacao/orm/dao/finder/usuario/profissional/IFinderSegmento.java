package br.com.waiso.autenticacao.orm.dao.finder.usuario.profissional;

import br.com.waiso.autenticacao.orm.modelo.usuario.profissional.Segmento;
import br.com.waiso.persistencia.interfaces.IFinder;

public interface IFinderSegmento extends IFinder<Long, Segmento> {
	
}
