package br.com.waiso.autenticacao.orm.modelo;

public enum NivelHierarquico {

	ADMINISTRADOR,
	COORDENADOR,
	DIRETOR,
	GERENTE,
	OPERADOR,
	PRESIDENTE,
	SUPERVISOR,
	USUARIO,
	VICE_PRESIDENTE;
	
}
