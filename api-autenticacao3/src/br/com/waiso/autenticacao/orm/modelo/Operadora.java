package br.com.waiso.autenticacao.orm.modelo;

public enum Operadora {

	CLARO,
	NEXTEL,
	OI,
	TIM,
	VIVO
	
}
