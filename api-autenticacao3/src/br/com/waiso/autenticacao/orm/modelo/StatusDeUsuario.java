package br.com.waiso.autenticacao.orm.modelo;

public enum StatusDeUsuario {

	OK,
	BLOQUEADO,
	BLOQUEADO_POR_SENHA_INVALIDA;
	
}
