package br.com.waiso.autenticacao.orm.modelo;

public enum TipoTelefone {

	RESIDENCIAL,
	COMERCIAL,
	CELULAR
	
}
