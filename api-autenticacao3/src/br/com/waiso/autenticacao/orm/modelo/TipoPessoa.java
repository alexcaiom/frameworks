package br.com.waiso.autenticacao.orm.modelo;

public enum TipoPessoa {

	PESSOA_FISICA,
	PESSOA_JURIDICA
	
}
