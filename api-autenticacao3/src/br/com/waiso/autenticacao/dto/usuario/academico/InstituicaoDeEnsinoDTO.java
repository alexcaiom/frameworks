package br.com.waiso.autenticacao.dto.usuario.academico;

import br.com.waiso.autenticacao.dto.EnderecoDTO;
import br.com.waiso.autenticacao.orm.modelo.usuario.academico.InstituicaoDeEnsino;

public class InstituicaoDeEnsinoDTO extends InstituicaoDeEnsino {
	
	private EnderecoDTO endereco;

	public EnderecoDTO getEndereco() {
		return endereco;
	}

	public void setEndereco(EnderecoDTO endereco) {
		this.endereco = endereco;
	}

}
